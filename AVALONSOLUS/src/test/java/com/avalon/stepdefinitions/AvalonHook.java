package com.avalon.stepdefinitions;

import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.Driver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class AvalonHook {
	
	@Before
	public void driverSetUp() {
		
		Driver.startDriver();
		
	}
	
	
	@After
	public void takeScreenshotIfFailed(Scenario scenario) {
		
		if(scenario.isFailed()) {
			
		BrowseUtils.takeScreenshotMethod(Driver.getDriver());
		
		}
		
		Driver.closeDriver();
	}

}
